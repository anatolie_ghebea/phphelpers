<?php 

namespace PHPHelpers;

class Util {

	
	/**
	 * Check if the given string date is of type 'd/m/Y'
	 */
	public static function isValidDateDMY( $inputDate ) {

		$inputDate = htmlspecialchars_decode($inputDate);
		if( strlen( $inputDate ) < 10 ) {
			return false;
		} else {
			$date = substr($inputDate, 0 , 10 );
			$bits = explode("/", $date);
			
			if(count($bits) < 3 ){
				return false;
			}

			$day = $bits[0];
			$month = $bits[1];
			$year = $bits[2];

			if( strlen($day) < 2 || !is_numeric($day)  || $day > 31 || $day < 1 ){
				return false;
			} 

			if( strlen($month) < 2 || !is_numeric($month)  || $month > 12 || $month < 1 ){
				return false;
			} 

			if( strlen($year) < 4 || !is_numeric($year) ){
				return false;
			} 

			return $date;
		}

	}
	
	/**
	 * Check if the given string date is of type 'd/m/Y'
	 */
	public static  function isValidDateYmd( $inputDate ) {
	
		$inputDate = htmlspecialchars_decode($inputDate);
		if( strlen( $inputDate ) < 10 ) {
			return false;
		} else {
			$date = substr($inputDate, 0 , 10 );
			$bits = explode("-", $date);
	
			if(count($bits) < 3 ){
				return false;
			}

			$day = $bits[2];
			$month = $bits[1];
			$year = $bits[0];
	
			if( strlen($day) < 2 || !is_numeric($day)  || $day > 31 || $day < 1 ){
				return false;
			} 
	
			if( strlen($month) < 2 || !is_numeric($month)  || $month > 12 || $month < 1 ){
				return false;
			} 
	
			if( strlen($year) < 4 || !is_numeric($year) ){
				return false;
			} 
			
			return $date;
		}
	
	}
	
	
	
	/**
	 * 
	 */
	public static  function dmyDate($date, $format = 'd/m/Y'){
	
		if( !$date )
			return '';
	
		if( $date instanceof \Carbon\Carbon ){
			return $date->format($format);
		} else {
			if( $date = self::isValidDateYmd($date) ) {
				return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($format);
			} else if( $date = self::isValidDateDMY($date) ) {
				return $date;
			}
		}
	
		return '';
	}
	
	
	

}