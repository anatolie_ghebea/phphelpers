# PHP Helpers 

This is a set of generic PHP files that contains definition of constants functions and classes that are generally used across Laravel projects.
By including this repository as a git submodule, we aim to improve consistency across the projects.

# Installation 

To be able to use this collection under a Laravel project, the following steps must be performed.

- move into `app/` of your laravel project
- add this repository as a git submodule `git submodule add -b production https://anatolie_ghebea@bitbucket.org/anatolie_ghebea/phphelpers.git`
- reanme the folder `mv phphelpers PHPHelpers`
- edit the section `autoload` in the `composer.json` in order for the composer to autoload the files. It should look like this 
 
        "autoload": {
            ...
            "psr-4": {
                "App\\": "app/",
                "PHPHelpers\\": "app/PHPHelpers"
            },
            ...
        },
- run `composer dump-autoload` to update the list of the autloaded files.

This should be enough.


# Extending 

If this collection is missing a funcitonality that might be useful in the other project, edit this respository than run  `git pull origin production` in the submodule directory 