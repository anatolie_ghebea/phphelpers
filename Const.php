<?php 

namespace PHPHelpers;

/**
 * JSON response constants
 */
define('RESP_STATUS', 'status');
define('RESP_MSG', 'message');
define('RESP_ERR', 'error');
define('RESP_DATA', 'data');